package ru.tsc.denisturovsky.tm.appconstant;

public final class TerminalConstant {

    public final static String ABOUT = "about";

    public final static String HELP = "help";

    public final static String VERSION = "version";

    public final static String HELP_SHORT = "-h";

    public final static String VERSION_SHORT = "-v";

    public final static String WELCOME = "** WELCOME TO TASK MANAGER **";

    public final static String FIRST_NAME = "Denis";

    public final static String LAST_NAME = "Turovsky";

    public final static String EMAIL = "dturovsky@t1-consulting.ru";

}
