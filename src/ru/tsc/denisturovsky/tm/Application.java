package ru.tsc.denisturovsky.tm;

import ru.tsc.denisturovsky.tm.appconstant.TerminalConstant;

import java.util.Locale;

public final class Application {

    public static void main(final String[] args) {
        process(args);
    }

    public static void process(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;

        showWelcome();
        switch (arg) {
            case TerminalConstant.ABOUT:
                showAbout();
                break;
            case TerminalConstant.HELP:
            case TerminalConstant.HELP_SHORT:
                showHelp();
                break;
            case TerminalConstant.VERSION:
            case TerminalConstant.VERSION_SHORT:
                showVersion();
                break;
            default:
                showError(arg);
                break;
        }
    }

    public static void showError(final String arg) {
        System.err.format("[Error] \n");
        System.err.format("This argument '%s' not supported", arg);
        System.exit(0);
    }

    public static void showVersion() {
        System.out.format("[%s] \n", TerminalConstant.VERSION.toUpperCase(Locale.ROOT));
        System.out.format("1.2.0");
        System.exit(0);
    }

    public static void showHelp() {
        System.out.format("[%s] \n", TerminalConstant.HELP.toUpperCase(Locale.ROOT));
        System.out.format("%s - Show developer info \n", TerminalConstant.ABOUT);
        System.out.format("%s - Show list of terminal commands \n", TerminalConstant.HELP);
        System.out.format("%s - Show program version", TerminalConstant.VERSION);
        System.exit(0);
    }

    public static void showAbout() {
        System.out.format("[%s] \n", TerminalConstant.ABOUT.toUpperCase(Locale.ROOT));
        System.out.format("Name: %s %s \n", TerminalConstant.FIRST_NAME, TerminalConstant.LAST_NAME);
        System.out.format("E-mail: %s", TerminalConstant.EMAIL);
        System.exit(0);
    }

    public static void showWelcome() {
        System.out.format("%s \n", TerminalConstant.WELCOME);
    }

}
